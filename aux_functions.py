import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from IPython.display import display # Allows the use of display() for DataFrames



        
def PrintHistogramOfDataset(dd,cols):
    dd = dd.select_dtypes(exclude =('object','uint8')) 
    for i in cols:
        plt.figure(figsize=(5,5))
        sns.distplot(dd[i])
        
        
def PrintHistogramOfDataset_Compare(dd1,dd2,cols):
    fig = plt.figure()
    dd1 = dd1.select_dtypes(exclude =('object','uint8')) 
    dd2 = dd2.select_dtypes(exclude =('object','uint8')) 
    for i in cols:
        plt.figure(figsize=(5,5))
        sns.distplot(dd1[i],color ='green')
        sns.distplot(dd2[i],color ='blue')
        fig.show()

        

def RemoveOutliersFromDataset(log_data):

    import itertools
    
    # Select the indices for data points you wish to remove
    outliers_lst  = []
    
    # For each feature find the data points with extreme high or low values
    for feature in log_data.columns:
        # TODO: Calculate Q1 (10th percentile of the data) for the given feature
        Q1 = np.percentile(log_data.loc[:, feature], 10)
    
        # TODO: Calculate Q3 (90th percentile of the data) for the given feature
        Q3 = np.percentile(log_data.loc[:, feature], 90)
    
        # TODO: Use the interquartile range to calculate an outlier step (1.5 times the interquartile range)
        step = 1.5 * (Q3 - Q1)
    
        # The tilde sign ~ means not
        # So here, we're finding any points outside of Q1 - step and Q3 + step
        outliers_rows = log_data.loc[~((log_data[feature] >= Q1 - step) & (log_data[feature] <= Q3 + step)), :]
    
        outliers_lst.append(list(outliers_rows.index))
    
    outliers = list(itertools.chain.from_iterable(outliers_lst))
    
    # List of unique outliers
    # We use set()
    # Sets are lists with no duplicate entries
    uniq_outliers = list(set(outliers))
    
    # List of duplicate outliers
    dup_outliers = list(set([x for x in outliers if outliers.count(x) > 1]))
    
    # Remove duplicate outliers
    # Only 5 specified
    #good_data = log_data.drop(log_data.index[dup_outliers]).reset_index(drop = True)
    good_data = log_data.drop(log_data.index[list(set(outliers))]).reset_index(drop = True)

    
    return good_data


def DBSCAN1(data,xCol,yCol):
    
    from  sklearn.cluster import DBSCAN
    from sklearn.preprocessing import StandardScaler
    import matplotlib.pyplot as plt
    
    
    
    data2 = data[[xCol, yCol]]
    data2 = data2.as_matrix().astype("float32", copy = False)
    
    #per stampare i valori trovati finora
    plt.figure(figsize=(15,15))
    plt.scatter(data2[:,0],data2[:,1], s = 10, c = 'red')
    plt.xlabel(xCol)
    plt.ylabel(yCol)
    plt.show()
    
    
    
    dbsc = DBSCAN(eps = 0.6, min_samples = 8).fit(data2)
    labels = dbsc.labels_
    core_samples = np.zeros_like(labels, dtype = bool)
    core_samples[dbsc.core_sample_indices_] = True
    
    
    
    # Number of clusters in labels, ignoring noise if present.   --- parte aggiunta
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    
    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    
    
    
    
    
    #fino a qua ok, ora dobbiamo stampare i valori che sono corratti con un colore, 
    #mentre quelli che sono considerati rumore devono  essere evidenziati con un altro colore
    
    core_samples = pd.DataFrame(core_samples)
    data2 = pd.DataFrame.from_records(data2)
    dataTMP = pd.concat([data2,core_samples],axis=1)
    dataTMP.columns = [xCol, yCol,'TF']
    
    #i dati che devono essere stampai come true vengono salvati in dataTrue
    dataTrue= dataTMP[dataTMP.TF == True ]
    dataTrue.drop(['TF'], axis = 1, inplace = True)
    dataTrue = dataTrue.as_matrix().astype("float32", copy = False)
    
    #i dati che devono essere stampati come false vengono salvati in dataFalse
    dataFalse= dataTMP[dataTMP.TF == False ]
    dataFalse.drop(['TF'], axis = 1, inplace = True)
    dataFalse = dataFalse.as_matrix().astype("float32", copy = False)
    
    plt.figure(figsize=(15,15))
    plt.scatter(dataFalse[:,0],dataFalse[:,1], s = 10, c = 'red')
    plt.scatter(dataTrue[:,0],dataTrue[:,1], s = 10, c = 'green')
    
    plt.xlabel(xCol)
    plt.ylabel(yCol)
    plt.show()   
    
    data2.mean(axis=0)
    data2.sum(axis = 0)
    data2 = pd.DataFrame.from_records(data2)
    data2.info()
    
    
def DBSCAN2(data, xCol, yCol,epsilon,minimum):
    
    import numpy as np
    from sklearn.cluster import DBSCAN
    from sklearn import metrics
    from sklearn.preprocessing import StandardScaler
    
    X = StandardScaler().fit_transform(data[[xCol, yCol]])
    
    # Compute DBSCAN - gioca con eps e min_samples per regolare n_clusters e n_noise
    db = DBSCAN(eps=epsilon, min_samples=minimum).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_
    
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    
    #print('Estimated number of clusters: %d' % n_clusters_)
    #print('Estimated number of noise points:', n_noise_,' out of ', data.shape[0],' => ',int((n_noise_/data.shape[0])*100),'% of values removed' )

    GraphTitle = 'DBSCAN --- Clusters: ' + str(n_clusters_) + '  ---  '+ 'Number of noise points:' +str(n_noise_) + ' out of ' + str(data.shape[0]) + ' => ' + str(int((n_noise_/data.shape[0])*100)) + '% of values removed'
    
    # Plot result
    import matplotlib.pyplot as plt
    
    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]
    plt.figure(figsize=(10,10))
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise. It is an RGBA color, meaning that the last value is the opacity
            col = [0, 0, 0, 0.5]
            #col = [0, 0, 0, 1]

    
        class_member_mask = (labels == k)
    
        #you can find color here https://www.mathworks.com/help/matlab/ref/linespec.html
        # 'o' means that yoiu want to plot a circle
        
        xy = X[class_member_mask & ~core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], '*', markerfacecolor=tuple(col),
                  markeredgecolor='k',markersize=4)
        
        xy = X[class_member_mask & core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=10)
    
        
    
    #plt.title('Estimated number of clusters: %d' % n_clusters_,' --- ciao')
    plt.title(GraphTitle)
    plt.xlabel(xCol)
    plt.ylabel(yCol)
    plt.show()
    
    
def PCAMethod(data,features,targ):
        #-----------------PCA METHOD  
    from sklearn.preprocessing import StandardScaler
    import matplotlib.pyplot as plt
    print("\n\n\n\n\n\n\n\n\n\n\n\n\nPCA\n\n\n\n\n\n")
    
    # Separating out the features
    x = data.loc[:, features].values
    # Separating out the target
    y = data.loc[:,[targ]].values
    #Trasforma y object in Dataframe per poterlo vedere
    #y = pd.DataFrame(y)
    # Standardizing the features
    x = StandardScaler().fit_transform(x)
    
    #Stampa i valori distinti di CLASS
    #data.CLASS.unique()
    
    #PCA PROJECTION to 2D
    from sklearn.decomposition import PCA
    
    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(x)
    principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
    
    #Concatenamento con la colonna CLASS
    finalDf = pd.concat([principalDf, data[[targ]]], axis = 1)
    
    #Visualizzo la 2D PROJECTION
    fig = plt.figure(figsize = (13,10))
    ax = fig.add_subplot(1,1,1) 
    ax.set_xlabel('Principal Component 1', fontsize = 15)
    ax.set_ylabel('Principal Component 2', fontsize = 15)
    ax.set_title('PCA plot shows clusters of samples based on their similarity', fontsize = 20)
    #targets = ['Silver', 'Tin', 'Gold', 'Platinum']
    #targets = [0,1,2,3]
    
    targets = np.unique(y).tolist() 

    
    colors = ['r', 'g', 'b', 'y']
    feature_vectors = pca.components_.T
    x = pd.DataFrame(x)
    # using scaling factors to make the arrows
    arrow_size, text_pos = 7.0, 8.0,
    for target, color in zip(targets,colors):
        indicesToKeep = finalDf[targ] == target
        ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                   , finalDf.loc[indicesToKeep, 'principal component 2']
                   , c = color
                   , s = 5)
        # projections of the original features
        for i, v in enumerate(feature_vectors):
            ax.arrow(0, 0, arrow_size*v[0], arrow_size*v[1], head_width=0.2, head_length=0.2, linewidth=2, color='red')
            ax.text(v[0]*text_pos, v[1]*text_pos, features[i], color='black', ha='center', va='center', fontsize=12)
    
    ax.legend(targets)
    ax.grid()
    
    print("\n[0:'Silver', 1:'Tin', 2:'Gold', 3:'Platinum']\n\n")
    
    print("See how these vectors are pinned at the origin of PCs (PC1 = 0 and PC2 = 0)? \
          Their project values on each PC show how much weight they have on that PC. \
          In this example, LTIME and AFFL strongly influence PC1, while BILL have \
          more say in PC2.\
          Another nice thing about loading plots: the angles between the vectors tell us \
          how characteristics correlate with one another. \
          When two vectors are close, forming a small angle, the two variables they represent\
          are positively correlated. Example: BILL and LTIME\
          If they meet each other at 90°, they are not likely to be correlated. Example: BILL and AFFL.\
          When they diverge and form a large angle (close to 180°), they are negative correlated: NEIGHBORHOOD and BILL. ")
    
    
def Elbow(data,col_name):
    km_data = data.loc[:, col_name].values
    from sklearn.cluster import KMeans
    wcss = []
    for i in range(1, 15):
        kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 0)
        kmeans.fit(km_data)
        wcss.append(kmeans.inertia_)
    plt.plot(range(1, 15), wcss)
    plt.title('The Elbow Method')
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS')
    plt.show()

def Silhouette(data,col_name):
    
    #--------------PCA (Principal Component Analysis) --- DIMENSIONALITY REDUCTION
    from sklearn.decomposition import PCA
    
    dataPCA = data.loc[:, col_name].values
    #dataPCA = data.iloc[:, [8,9]].values
    
    # TODO: Apply PCA by fitting the good data with only two dimensions
    pca = PCA(n_components=2).fit(dataPCA)
    
    # TODO: Transform the good data using the PCA fit above
    reduced_data = pca.transform(dataPCA)
    
    # TODO: Transform log_samples using the PCA fit above
    pca_samples = pca.transform(dataPCA)
    
    # Create a DataFrame for the reduced data
    reduced_data = pd.DataFrame(reduced_data, columns = ['Dimension 1', 'Dimension 2'])
    
    # Display sample log-data after applying PCA transformation in two dimensions
    display(pd.DataFrame(np.round(pca_samples, 4), columns = ['Dimension 1', 'Dimension 2']))
    
    
    
    
    #----------------using the silhouette method
    n_clusters = [6,5,4,3,2]
    
    from sklearn.mixture import GaussianMixture
    from sklearn.metrics import silhouette_score
    
    nbs = []
    
    for n in n_clusters:
        
        # TODO: Apply your clustering algorithm of choice to the reduced data 
        clusterer = GaussianMixture(n_components=n).fit(reduced_data)
    
        # TODO: Predict the cluster for each data point
        preds = clusterer.predict(reduced_data)
    
        # TODO: Find the cluster centers
        centers = clusterer.means_
    
        # TODO: Predict the cluster for each transformed sample data point
        sample_preds = clusterer.predict(pca_samples)
    
        # TODO: Calculate the mean silhouette coefficient for the number of clusters chosen
        score = silhouette_score(reduced_data,preds)
        
        nbs.append(score)
        
        print ("\nThe silhouette_score for {} clusters is {}".format(n,score)) 
        
    print ("\nThe better number of clusters is {}".format(n_clusters[nbs.index(max(nbs))]))
    
    
    
def K_Means(data, col_name, clusters):
    
    import matplotlib.pyplot as plt
    from sklearn.cluster import KMeans

    
    # Importing the dataset
    km_data = data.loc[:, col_name].values
    
    #X = StandardScaler().fit_transform(X)
    
    # Fitting K-Means to the dataset
    kmeans = KMeans(n_clusters = clusters, init = 'k-means++', random_state = 0)
    y_kmeans = kmeans.fit_predict(km_data)
    
    # Visualising the clusters
    plt.figure(figsize=(10,10))
    if clusters == 1 :
        plt.scatter(km_data[y_kmeans == 0, 0], km_data[y_kmeans == 0, 1], s = 10, c = 'red', label = 'Cluster 0')
    elif clusters == 2 :
        plt.scatter(km_data[y_kmeans == 0, 0], km_data[y_kmeans == 0, 1], s = 10, c = 'red', label = 'Cluster 0')
        plt.scatter(km_data[y_kmeans == 1, 0], km_data[y_kmeans == 1, 1], s = 10, c = 'blue', label = 'Cluster 1')
    elif clusters == 3 :
        plt.scatter(km_data[y_kmeans == 0, 0], km_data[y_kmeans == 0, 1], s = 10, c = 'red', label = 'Cluster 0')
        plt.scatter(km_data[y_kmeans == 1, 0], km_data[y_kmeans == 1, 1], s = 10, c = 'blue', label = 'Cluster 1')
        plt.scatter(km_data[y_kmeans == 2, 0], km_data[y_kmeans == 2, 1], s = 10, c = 'green', label = 'Cluster 2')
    elif clusters == 4 :
        plt.scatter(km_data[y_kmeans == 0, 0], km_data[y_kmeans == 0, 1], s = 10, c = 'red', label = 'Cluster 0')
        plt.scatter(km_data[y_kmeans == 1, 0], km_data[y_kmeans == 1, 1], s = 10, c = 'blue', label = 'Cluster 1')
        plt.scatter(km_data[y_kmeans == 2, 0], km_data[y_kmeans == 2, 1], s = 10, c = 'green', label = 'Cluster 2')
        plt.scatter(km_data[y_kmeans == 3, 0], km_data[y_kmeans == 3, 1], s = 10, c = 'cyan', label = 'Cluster 3')
    elif clusters == 5 :
        plt.scatter(km_data[y_kmeans == 0, 0], km_data[y_kmeans == 0, 1], s = 10, c = 'red', label = 'Cluster 0')
        plt.scatter(km_data[y_kmeans == 1, 0], km_data[y_kmeans == 1, 1], s = 10, c = 'blue', label = 'Cluster 1')
        plt.scatter(km_data[y_kmeans == 2, 0], km_data[y_kmeans == 2, 1], s = 10, c = 'green', label = 'Cluster 2')
        plt.scatter(km_data[y_kmeans == 3, 0], km_data[y_kmeans == 3, 1], s = 10, c = 'cyan', label = 'Cluster 3')
        plt.scatter(km_data[y_kmeans == 4, 0], km_data[y_kmeans == 4, 1], s = 10, c = 'black', label = 'Cluster 4')
    elif clusters == 6 :
        plt.scatter(km_data[y_kmeans == 0, 0], km_data[y_kmeans == 0, 1], s = 10, c = 'red', label = 'Cluster 0')
        plt.scatter(km_data[y_kmeans == 1, 0], km_data[y_kmeans == 1, 1], s = 10, c = 'blue', label = 'Cluster 1')
        plt.scatter(km_data[y_kmeans == 2, 0], km_data[y_kmeans == 2, 1], s = 10, c = 'green', label = 'Cluster 2')
        plt.scatter(km_data[y_kmeans == 3, 0], km_data[y_kmeans == 3, 1], s = 10, c = 'cyan', label = 'Cluster 3')
        plt.scatter(km_data[y_kmeans == 4, 0], km_data[y_kmeans == 4, 1], s = 10, c = 'black', label = 'Cluster 4')
        plt.scatter(km_data[y_kmeans == 5, 0], km_data[y_kmeans == 5, 1], s = 10, c = 'darksalmon', label = 'Cluster 5')

    
    #plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
    #plt.scatter(km_data[y_kmeans == clusters, 0], km_data[y_kmeans == clusters, 1], s = 100, c = 'magenta', label = 'Cluster 5')
    plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 50, c = 'yellow', label = 'Centroids')
    plt.title('Clusters of customers')
    plt.xlabel(col_name[0])
    plt.ylabel(col_name[1])
    plt.legend()
    plt.show()
   

def HierarchicalClustering(data,col_name):

    from sklearn.preprocessing import StandardScaler
    X = StandardScaler().fit_transform(data[col_name])
    
    
    # Using the dendrogram to find the optimal number of clusters
    import scipy.cluster.hierarchy as sch
    plt.figure(figsize = (14,5))
    dendrogram = sch.dendrogram(sch.linkage(X, method = 'ward'))
    plt.title('Dendrogram')
    plt.xlabel('Customers')
    plt.ylabel('Euclidean distances')
    plt.show()
    
    #Data per hc
    #hc_data = data[["AFFL","LTIME"]]
    
    from sklearn import preprocessing
    hc_data = data.loc[:, col_name].values
    
    hc_data = preprocessing.scale(hc_data)
    
    # Fitting Hierarchical Clustering to the dataset
    from sklearn.cluster import AgglomerativeClustering
    hc = AgglomerativeClustering(n_clusters = 4, affinity = 'euclidean', linkage = 'ward')
    y_hc = hc.fit_predict(hc_data)
    
    # Visualising the clusters
    plt.figure(figsize=(10,5))
    #plt.scatter(X[:,0], X[:,1], c=hc.labels_, cmap='rainbow')  
    plt.scatter(hc_data[y_hc == 0, 0], hc_data[y_hc == 0, 1], s = 10, c = 'red', label = 'Sceptic')
    plt.scatter(hc_data[y_hc == 1, 0], hc_data[y_hc == 1, 1], s = 10, c = 'blue', label = 'Loyal')
    plt.scatter(hc_data[y_hc == 2, 0], hc_data[y_hc == 2, 1], s = 10, c = 'green', label = 'Enthusiast')
    plt.scatter(hc_data[y_hc == 3, 0], hc_data[y_hc == 3, 1], s = 10, c = 'cyan', label = 'Moderate')
    #plt.scatter(hc_data[y_hc == 4, 0], hc_data[y_hc == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
    plt.title('Clusters of customers')
    #plt.xlabel('Annual Income (k$)')
    #plt.ylabel('Spending Score (1-100)')
    plt.legend()
    plt.show()


