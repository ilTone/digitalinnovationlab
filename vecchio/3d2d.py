
# NON ESEGUIRE (TENERE PER SCOPI FUTURI)

# ##############Mean shift clustering - 3D PLOT######################
print("\n\n\n\n\n\n\n\n\n\n\n\n\nMean shift clustering\n\n\n\n\n\n")

"""Cerchiamo di capire se si può plottare qualcosa in 3d : magari hc viene meglio"""
import numpy as np
from sklearn.cluster import MeanShift
from sklearn.datasets.samples_generator import make_blobs
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import style
style.use("ggplot")

#centers = [[1,1,1],[5,5,5],[3,10,10]]

#X, _ = make_blobs(n_samples = 500, centers = centers, cluster_std = 1.5)
X = data[["LTIME", "AFFL", "BILL"]].values

ms = MeanShift()
ms.fit(X)               # si impalla!!!!!!
labels = ms.labels_
cluster_centers = ms.cluster_centers_

print(cluster_centers)

n_clusters_ = len(np.unique(labels))

print("Number of estimated clusters: %d" % n_clusters_)

# ##############3D PLOT######################
colors = 10*['r','g','b','c','k','y','m']

print(colors)
print(labels)

fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(111, projection='3d')

for i in range(len(X)):

    ax.scatter(X[i][0], X[i][1], X[i][2], c=colors[labels[i]], marker='o')


ax.scatter(cluster_centers[:,0],cluster_centers[:,1],cluster_centers[:,2],
            marker="x",color='k', s=150, linewidths = 5, zorder=10)

plt.show()
# ##############3D PLOT######################
# ##############2D PLOT######################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
for k, col in zip(range(n_clusters_), colors):
    my_members = labels == k
    cluster_center = cluster_centers[k]
    plt.plot(X[my_members, 0], X[my_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()
# ##############2D PLOT############################