
# Import libraries necessary for this project
import numpy as np
import pandas as pd
from IPython.display import display # Allows the use of display() for DataFrames

import aux_functions as af #Library written by us in order to keep code clean 


print("\n\n\n\n\n\n\n\n\n\n\n\n\nPREPROCESSING\n\n\n\n\n\n")

data = pd.read_csv("organics.csv")

data.columns
data.shape
data.info()
data.drop(['AGEGRP1', 'AGEGRP2','Unnamed: 0','TV_REG','NGROUP'], axis = 1, inplace = True)

#stampo quanti dati nulli ci sono
data.isnull().sum(axis = 0)

data.columns

data = data[pd.notnull(data['CUSTID'])]
data = data[pd.notnull(data['GENDER'])]
data = data[pd.notnull(data['DOB'])]
data = data[pd.notnull(data['EDATE'])]
data = data[pd.notnull(data['AGE'])]
data = data[pd.notnull(data['NEIGHBORHOOD'])]
data = data[pd.notnull(data['LCDATE'])]
data = data[pd.notnull(data['ORGANICS'])]
data = data[pd.notnull(data['BILL'])]
data = data[pd.notnull(data['REGION'])]
data = data[pd.notnull(data['ORGYN'])]
data = data[pd.notnull(data['AFFL'])]
data = data[pd.notnull(data['LTIME'])]
data = data[pd.notnull(data['CLASS'])]

#ripulisce la colonna del "GENDER", 
#rimuovendo tutte le righe contenenti valori NaN oppure "U"
data = data[data.GENDER != 'U' ]

data.shape

data.isnull().sum(axis = 0)


"""
    this part is for converting dates written as strings to the "1994-33-22" format
    so that they become a whole with only the year -> 1994
"""
#this part is used to convert dates 
data['DOB'] = data['DOB'].map(lambda x:  int(x[:x.index("-")])    )
data['EDATE'] = data['EDATE'].map(lambda x:  int(x[:x.index("-")])    )
data['LCDATE'] = data['LCDATE'].map(lambda x:  int(x[:x.index("-")])    )

"""
    this part serves for the dummies, i.e. to form multiple columns so as not to 
    have a wrong analysis of the dataset because of the previous label_encoding,
    as this has generated several numeric classes
"""

dataTMP = pd.get_dummies(data["GENDER"],prefix="GENDER_")
data = pd.concat([data,dataTMP],axis=1)
data.drop(['GENDER'], axis = 1, inplace = True)

dataTMP = pd.get_dummies(data["ORGANICS"],prefix="ORGANICS_")
data = pd.concat([data,dataTMP],axis=1)
data.drop(['ORGANICS'], axis = 1, inplace = True)

dataTMP = pd.get_dummies(data["REGION"],prefix="REGION_")
data = pd.concat([data,dataTMP],axis=1)
data.drop(['REGION'], axis = 1, inplace = True)

dataTMP = pd.get_dummies(data["ORGYN"],prefix="ORGYN_")
data = pd.concat([data,dataTMP],axis=1)
data.drop(['ORGYN'], axis = 1, inplace = True)

#used for OneHotEncoding of CLASS (convert string into numbers)
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder = LabelEncoder()
data[['CLASS']] = labelencoder.fit_transform(data[['CLASS']])


#necessary to see range of values we have
for column in data:
    print(column  ," ->   MAX: ", data[column].unique().max()," MIN: ", data[column].unique().min())


#---------------------------------------------------------PRINT


#questa parte serve per fare l'analisi dei dati e stampare i risultati ottenuti
#source: https://realpython.com/python-histograms/

import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt

sns.set_style('darkgrid')
  
#plot dei dati iniziali
print("plot dei dati iniziali")
col_name = ['AFFL','LTIME','BILL','AGE']
af.PrintHistogramOfDataset(data,col_name)

#questa parte serve per standardizzare i dati
from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
dataStandard = sc.fit_transform(data)
dataStandard = pd.DataFrame(dataStandard)
dataStandard.columns = data.columns

# plot dei dati standardizzati
print("plot dei dati standardizzati")
af.PrintHistogramOfDataset(dataStandard,col_name)

#ora devo levare i dati che sono di troppo, ovvero l'outlier detection

dataStandard_NoOutl = af.RemoveOutliersFromDataset(dataStandard)

# plot dei dati standardizzati senza outliers
print("plot dei dati standardizzati senza outliers")
af.PrintHistogramOfDataset(dataStandard_NoOutl,col_name)

print("plot dei dati standardizzati senza + con outliers")
af.PrintHistogramOfDataset_Compare(dataStandard,dataStandard_NoOutl,col_name)

#---------------------------------------------------------FINE STAMPA

#Using DBSCAN on those columns

#Variamo eps a 0.28 per avere due clusters di cui uno con soli 100 campioni
#Appena arriviamo a 0.3 si crea un solo cluster
af.DBSCAN2(data,"AGE", "BILL", 0.28, 100)

#TODO
af.DBSCAN2(data, "AFFL","BILL", 0.3, 100)


#TODO
af.DBSCAN2(data, "LTIME","BILL", 0.6, 100)

#applico il PCA per vedere quali sono le colonne piu correlate
features = ['LTIME', 'AFFL', 'BILL','AGE', 'NEIGHBORHOOD']
target = 'CLASS'
af.PCAMethod(data,features,target)


#Run of AFFL & LTIME
col_name = ['AFFL','LTIME']
# Using the elbow method to find the optimal number of clusters
af.Elbow(data,col_name)

#Using the silhouette method
#af.Silhouette(data,col_name)

#Using K-Means
#graphically we can see that the best number or clusters is 4
#nb_cluster = input('Prompt number of Clusters : ')
af.K_Means(data,col_name, 4)

#Using Hierarchical Clustering
#af.HierarchicalClustering(data,col_name)

col_name.clear()



#Run of AFFL & LTIME cleared by outliers
col_name = ['AFFL','LTIME']
# Using the elbow method to find the optimal number of clusters
af.Elbow(dataStandard_NoOutl,col_name)

#Using the silhouette method
#af.Silhouette(data,col_name)

#Using K-Means
#graphically we can see that the best number or clusters is 3
#nb_cluster = input('Prompt number of Clusters : ')
af.K_Means(dataStandard_NoOutl,col_name, 3)

#Using Hierarchical Clustering
#af.HierarchicalClustering(data,col_name)

col_name.clear()




#Run of AGE & LTIME 
col_name = ['AGE','LTIME']
# Using the elbow method to find the optimal number of clusters
af.Elbow(data,col_name)

#Using the silhouette method
#af.Silhouette(data,col_name)

#Using K-Means
#graphically we can see that the best number or clusters is 3
#nb_cluster = input('Prompt number of Clusters : ')
af.K_Means(data,col_name, 3)

#Using Hierarchical Clustering
#af.HierarchicalClustering(data,col_name)

col_name.clear()





data.LTIME.unique()
data.BILL.unique().min()

data.BILL.count(data.BILL.unique().min())
cSonta = data.BILL.count
cSonta

data2 = data[data.BILL > 15000  ]




X = data.loc[:,['LTIME']].values
y = data.loc[:, ['BILL']].values


col_name = ["AFFL","BILL"]
af.K_Means(data2,col_name, 3)
af.DBSCAN2(data2, "AFFL","BILL", 0.3, 100)

from sklearn.model_selection import train_test_split 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)


# Fitting Linear Regression to the dataset
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)

# Visualizing the Linear Regression results
def viz_linear():
    plt.scatter(X, y, color='red')
    plt.plot(X, lin_reg.predict(X), color='blue')
    plt.title('Truth or Bluff (Linear Regression)')
    plt.xlabel('Position level')
    plt.ylabel('Salary')
    plt.show()
    return
viz_linear()



# Fitting Polynomial Regression to the dataset
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree=4)
X_poly = poly_reg.fit_transform(X)
pol_reg = LinearRegression()
pol_reg.fit(X_poly, y)

# Visualizing the Polymonial Regression results
def viz_polymonial():
    plt.scatter(X, y, color='red')
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
    plt.title('Truth or Bluff (Linear Regression)')
    plt.xlabel('LTIME')
    plt.ylabel('BILL')
    plt.show()
    return
viz_polymonial()








"""
#Loop for selecting which plot is the best to show
col_name = []
collection = ['AGE', 'BILL', 'AFFL', 'LTIME']
for x in collection:
    for y in collection:
        if (x!=y):
            col_name.append(x)
            col_name.append(y)
            print("\n\n")
            print(col_name)
            af.DBSCAN2(dataStandard, x, y)
            #af.Elbow(dataStandard,col_name)
            #nb_cluster = input('Prompt number of Clusters : ')
            #af.K_Means(dataStandard,col_name, int(nb_cluster))
            col_name.clear()
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

kmeans = KMeans(n_clusters = 4, init = 'k-means++', random_state = 0)
clusters = kmeans.fit_predict(dataStandard.loc[:,['BILL','AFFL','AGE','LTIME']])
clusters = kmeans.fit_predict(dataStandard)

#unisco i cluster ottenuti con i valori di CLASS
classes = pd.DataFrame(data.loc[:,'CLASS'])
clusters = pd.DataFrame(clusters)
#resetto gli indici per poi unire le due colonne
clusters = clusters.reset_index(drop=True)
classes = classes.reset_index(drop=True)
MatchClusterClasses = pd.concat([clusters,classes],axis=1)
MatchClusterClasses.columns = ['CLUSTER','CLASS']
MatchClusterClasses['CLUSTER'].corr(MatchClusterClasses['CLASS'])

#create a new dataframe which contains percentage for each class
size_tmp = pd.DataFrame(MatchClusterClasses.groupby(["CLUSTER", "CLASS"]).size())
perc_tmp = pd.DataFrame(size_tmp / size_tmp.groupby(level=1).sum())
Percentages = pd.concat([size_tmp,perc_tmp],axis=1).reset_index()
Percentages.columns = ['CLUSTER','CLASS','OCCURRENCY','PERCENTAGE']
#Percentages.drop(['OCCURRENCY'], axis = 1, inplace = True)
Percentages['PERCENTAGE'] *= 100

print(Percentages)



#uso PCA per avere una visualizzazione dei dati su due dimensioni
#in più nella stampa vengono anche rese visibili le CLASS
dataStandard['CLUSTER'] = clusters

reduced_data = PCA(n_components=2).fit_transform(dataStandard)
results = pd.DataFrame(reduced_data,columns=['pc1','pc2'])


af.Elbow(results,results.columns)

af.Silhouette(results,results.columns)

sns.scatterplot(x="pc1", y="pc2", data=results, s=20)
plt.title('K-means Clustering with 2 dimensions')
plt.show()

sns.scatterplot(x="pc1", y="pc2",hue=data['CLASS'], data=results, s=20)
plt.title('K-means Clustering with 2 dimensions')
plt.show()

sns.scatterplot(x="pc1", y="pc2", hue=dataStandard['CLUSTER'], data=results, s=20)
plt.title('K-means Clustering with 2 dimensions')
plt.show()

"""

#parte per far vedere la correlazione
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns



def correlation_heatmap(train):
    correlations = train.corr()

    fig, ax = plt.subplots(figsize=(10,10))
    sns.heatmap(correlations, vmax=1.0, center=0, fmt='.2f',
                square=True, linewidths=.5, annot=True, cbar_kws={"shrink": .70})
    plt.show();
    
#correlation_heatmap(data.loc[:,['BILL','AFFL','AGE','LTIME']])
correlation_heatmap(dataStandard_NoOutl)





sns.scatterplot(x="pc1", y="pc2", hue=dataStandard['CLUSTER'], data=results, s=20)
plt.title('K-means Clustering with 2 dimensions')
plt.show()

dataStandard.columns
 
for i in data.columns:
    sns.scatterplot(x="pc1", y="pc2", hue=data[i], data=results, s=20)
    plt.title('K-means Clustering with 2 dimensions')
    plt.show()
    
    
"""









