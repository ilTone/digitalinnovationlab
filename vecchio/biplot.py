#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 18:35:31 2019

@author: alessiotonelli
"""

# See https://github.com/teddyroland/python-biplot/blob/master/biplot.py

"""
DIMENSIONALITY REDUCION (PCA)

BIPLOT VISUALIZATION 
"""

import pandas as pd
from sklearn.decomposition import PCA
from matplotlib import pyplot as plt



data = pd.read_csv("Wholesalecustomersdata.csv")
data.drop(['Region', 'Channel'], axis = 1, inplace = True)
# if no row or column titles in your csv, pass 'header=None' into read_csv
# and delete 'index_col=0' -- but your biplot will be clearer with row/col names



## perform PCA

n = len(data.columns)

pca = PCA(n_components = n)
# defaults number of PCs to number of columns in imported data (ie number of
# features), but can be set to any integer less than or equal to that value

pca.fit(data)



## project data into PC space

# 0,1 denote PC1 and PC2; change values for other PCs
xvector = pca.components_[0] # see 'prcomp(my_data)$rotation' in R
yvector = pca.components_[1]

xs = pca.transform(data)[:,0] # see 'prcomp(my_data)$x' in R
ys = pca.transform(data)[:,1]

print(pca.explained_variance_ratio_) 

print(pca.singular_values_)


## visualize projections
    
## Note: scale values for arrows and text are a bit inelegant as of now,
##       so feel free to play around with them

plt.figure(figsize=(20,10))
for i in range(len(xvector)):
# arrows project features (ie columns from csv) as vectors onto PC axes
    plt.arrow(0, 0, xvector[i]*max(xs), yvector[i]*max(ys),
              color='r', width=0.0005, head_width=0.0025)
    plt.text(xvector[i]*max(xs)*1.01, yvector[i]*max(ys)*1.02,
             list(data.columns.values)[i], color='r')

for i in range(len(xs)):
# circles project documents (ie rows from csv) as points onto PC axes
    plt.plot(xs[i], ys[i], 'bo')
    #plt.text(xs[i]*1.2, ys[i]*1.2, list(dat.index)[i], color='g')


plt.xlabel("Dimension 1", fontsize=14)
plt.ylabel("Dimension 2", fontsize=14)
plt.title("PC plane with original feature projections.", fontsize=14);
plt.show()